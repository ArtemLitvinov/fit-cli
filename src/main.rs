extern crate rustbreak;
use std::env;
use std::process;
use fit_cli::CommandLine;
use fit_cli::Database;

fn main() {
    let args: Vec<String> = env::args().collect();

    let command = CommandLine::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing argument: {}", err);
        process::exit(1);
    });
    
    command.run();
    println!("{:?}", command);
}

