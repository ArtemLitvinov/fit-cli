use std::process;

#[derive(Debug)]
enum Command {
    Add,
    Remove,
    Activity,
    Stats
}

impl Command {
    fn from_string(command: &str) -> Option<Command> {
        match command {
            "add" => Some(Command::Add),
            "remove" => Some(Command::Remove),
            "activity" => Some(Command::Activity),
            "stats" => Some(Command::Stats),
            _ => None
        }
    }
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Activity {
    name: String,
    value: String
}

impl Activity {
    pub fn new( args: Vec<String>) -> Result<Activity, &'static str> {
        if args.len() < 2 {
            return Err("Not enough arguments")
        }
        let name = args[0].clone();
        let value  = args[1].clone();

        Ok ( Activity { name, value } )
    }
} 

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ActivityType {
    name: String,
    units: String
}

impl ActivityType {
    pub fn new( args: Vec<String>) -> Result<ActivityType, &'static str> {
        if args.len() < 2 {
            return Err("Not enough arguments")
        }
        let name = args[0].clone();
        let units  = args[1].clone();

        Ok ( ActivityType { name, units } )
    }
}

#[derive(Debug)]
pub struct CommandLine {
    command: Command,
    params: Vec<String>
}

impl CommandLine {
    pub fn new( args: &[String]) -> Result<CommandLine, &'static str> 
    {
        if let Some((_, strings)) = args.split_first() {
            if let Some((command, params)) = strings.split_first() {
                if let Some(comm) = Command::from_string(command) {
                    return Ok (CommandLine { command: comm , params: params.to_vec() })
                } else {
                    return Err("Wrong command")
                    }
            } else {
                return Err("Not enough arguments")
            }
        } else {
            return Err("Not enough arguments")
        }
    }
    pub fn run(&self) {
        let db = Database::init();
        match &self.command {
            Command::Add => {
                let param = &self.params.clone().to_vec();
                let activity_type = ActivityType::new(param.to_owned())
                    .unwrap_or_else(|err| {
                        println!("Activity type error: {}", err);
                        process::exit(1)
                    });
                db.write_new_activity_type(activity_type.clone())
                    .unwrap_or_else(|err| {
                        println!("Activity type error: {}", err);
                        process::exit(1)
                    });
                println!("Add activity type: {:?}", activity_type);
            },
            Command::Remove => {
                let param = &self.params.clone().to_vec();
                let activity_type = &param[0];
                db.remove_activity_type(activity_type)
                    .unwrap_or_else(|err| {
                        println!("Activity type error: {}", err);
                        process::exit(1)
                    });
                println!("Remove activity type: {}", activity_type);
            }
            Command::Activity => {
                let param = &self.params.clone().to_vec();
                let activity = Activity::new(param.to_owned())
                    .unwrap_or_else(|err| {
                        println!("Activity error: {}", err);
                        process::exit(1)
                });
                db.write_new_activity(activity.clone())
                    .unwrap_or_else(|err| {
                        println!("Activity error: {}", err);
                        process::exit(1)
                });
                println!("{:?}", activity);
            },
            Command::Stats => {
                db.get_stats().unwrap();
            }
        }
    }
}

use std::path::PathBuf;
use rustbreak::{PathDatabase, deser::Ron};
use rustbreak::error::{RustbreakError};
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Data {
    activity_types: Vec<ActivityType>,
    activity_list: Vec<Activity>
}

pub struct Database {
    database: PathDatabase<Data, Ron>
}

impl Database {
    pub fn init() -> Database {
        let db = PathDatabase::<Data, Ron>::load_from_path_or(
            PathBuf::from("./.fitdb"),
            Data {
                activity_list: Vec::new(),
                activity_types: Vec::new()
            }
        );

        println!("Database initialization");

        return Database { database: db.unwrap()}

    }

    pub fn write_new_activity_type (&self, activity_type: ActivityType) -> Result<&Database, &'static str> {
        let dbs = &self.database;

        let exist_check = dbs.read(|db| {
            let result = &db.activity_types;
            return result.into_iter().any(|act_type| { act_type.name == activity_type.name });
        }).unwrap();

        if exist_check {
            return Err("The activity type exists")
        }

        dbs.write(|db| {
            db.activity_types.push(activity_type)
        }).unwrap();

        self.database.save().unwrap();

        Ok(self)
    }

    pub fn remove_activity_type (&self, activity_type: &str) -> Result<&Database, &'static str> {
        let dbs = &self.database;

        let exist_check = dbs.read(|db| {
            let result = &db.activity_types;
            return result.into_iter().any(|act_type| { act_type.name == activity_type });
        }).unwrap();

        if exist_check {
            dbs.write(|db| {
                db.activity_types.retain(|act_type| { act_type.name != activity_type })
            }).unwrap();
            self.database.save().unwrap();

            return Ok(self)
        }

        Err("Unable to remove non-existent activity type")

    }

    pub fn write_new_activity(&self, activity: Activity) -> Result<&Database, &'static str> {
        let dbs = &self.database;

        let exist_check = dbs.read(|db| {
            let result = &db.activity_types;
            return result.into_iter().any(|act_type| { act_type.name == activity.name });
        }).unwrap();

        if exist_check {
            dbs.write(|db| {
                db.activity_list.push(activity)
            }).unwrap();

            self.database.save().unwrap();

            return Ok(self)
        }

        Err("Unable create activity of non-existent type")

    }

    pub fn get_stats(&self) -> Result<&Database, &'static str> {
        let dbs = &self.database;

        let stats = dbs.read(|db| {
            let types = &db.activity_types;
            let activities = &db.activity_list;

            types.into_iter()
                .for_each(|t| {
                    let amount = &activities
                        .into_iter()
                        .filter(|a| {
                            a.name == t.name
                        })
                        .map(|a| {
                            a.value.parse::<i32>().unwrap()
                        })
                        .reduce(|acc, el| {
                            acc + el
                        }).unwrap();
                    println!("Activity '{}': {} {}", t.name, amount, t.units)
                });
        });

        Ok(&self)
    }
}
